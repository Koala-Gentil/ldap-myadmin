module.exports = {
    ldap: {
        base: 'dc=bla,dc=com',
        user: 'cn=admin,dc=bla,dc=com',
        password: 'bla',
        url: 'ldap://ldap:389/dc=bla,dc=com',
        peopleOU: 'People',
        groupOU: 'Groups',
        adminGroup: 'LDAP_MANAGER_ADMIN', // cn=LDAP_MANAGER_ADMIN,ou=group,dc=example,dc=com
    },
    jwt: {
        secret: 'secret',
        hoursBeforeExp: 24
    }
};

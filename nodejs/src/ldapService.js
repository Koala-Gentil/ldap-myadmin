const LdapClient = require('ldapjs-client');
const ldapSettings = require('./settings').ldap;
const tokenService = require('./tokenService');
const settings = require('./settings');

const groupPath = `OU=${settings.ldap.groupOU},${settings.ldap.base}`;
const peoplePath = `OU=${settings.ldap.peopleOU},${settings.ldap.base}`;

class LdapService {
    constructor() {
        this.client = new LdapClient({ url: settings.ldap.url });
    }

    async init() {
        try {
            await this.client.bind(ldapSettings.user, ldapSettings.password);
        } catch (e) {
            console.error('Bind failed');
            throw e;
        }
    }

    async getAllUsers() {
        try {
            const entries = await this.client.search(peoplePath, {
                filter: '(objectClass=person)',
                scope: 'sub',
                attributes: ['uid', 'dn', 'sn', 'givenName', 'homeDirectory', 'cn'],
            });
            return entries;
        } catch (e) {
            console.error('ldapException', e);
            throw e;
        }
    }

    async getUser(uid) {
        try {
            const entries = await this.client.search(peoplePath, {
                filter: `(uid=${uid})`,
                scope: 'sub',
                attributes: ['uid', 'dn', 'sn', 'gidNumber', 'givenName', 'homeDirectory', 'cn'],
            });

            const entry = entries[0];

            if (!entry) {
                return null;
            } 

            const groups = await this.getUserGroups(uid);

            return {...entry, groups};

        } catch (e) {
            console.error('ldapException', e);
            throw e;
        }
    }

    _formatGroups(ldapGroups) {
        return ldapGroups.map(this._formatGroup);
    }

    _formatGroup(ldapGroup) {
        const members = Array.isArray(ldapGroup.memberUid) ? ldapGroup.memberUid : (ldapGroup.memberUid ? [ldapGroup.memberUid] : [])

        return {
            dn: ldapGroup.dn,
            cn: ldapGroup.cn,
            gidNumber: ldapGroup.gidNumber,
            members
        };
    }

    async getAllGroups() {
        try {
            const entries = await this.client.search(groupPath, {
                filter: '(objectClass=posixGroup)',
                scope: 'sub',
                attributes: ['dn', 'cn', 'gidNumber', 'memberUid'],
            });
            return this._formatGroups(entries);
        } catch (e) {
            console.error('ldapException', e);
            throw e;
        }
    }

    async getUserGroups(uid) {
        try {
            const entries = await this.client.search(groupPath, {
                filter: `(memberUid=${uid})`,
                scope: 'sub',
                attributes: ['cn'],
            });
            return entries.map(e => e.cn);
        } catch (e) {
            console.error('ldapException', e);
            throw e;
        }
    }

    async getGroup(cn) {
        try {
            const entries = await this.client.search(groupPath, {
                scope: 'sub',
                filter: `(cn=${cn})`,
                attributes: ['dn', 'cn', 'gidNumber', 'memberUid'],
            });

            const entry = entries[0];

            if (entry) {
                return this._formatGroup(entry);
            }
            return null
        } catch (e) {
            console.error('ldapException', e);
            return null;
        }
    }

    /**
     * Tente d'authentifier un utilisateur du LDAP
     * @param {string} username 
     * @param {string} password 
     * @returns {*} Jwt Token ou null si échec
     */
    async signIn(username, password) {
        try {
            const entries = await this.client.search(peoplePath, {
                filter: `uid=${username}`,
                scope: 'sub',
                attributes: ['userPassword', 'cn', 'uid']
            });

            if (entries.length === 1) {
                const user = entries[0];
                if (user.userPassword === password) {
                    const groups = await this.getUserGroups(user.uid)
                    const role = groups.includes(settings.ldap.adminGroup) ? 'ADMIN' : 'USER';
                    return await tokenService.generateToken({ cn: user.cn, role, username, uid: user.uid });
                }
            }
        } catch (e) {
            console.error('ldapException', e);
        }
        return null;
    }

    async addMemberToGroup(groupCn, userUid) {
        try {
            const change = {
                operation: 'add', // add, delete, replace
                modification: {
                    memberUid: userUid
                }
            };

            await this.client.modify(`cn=${groupCn},${groupPath}`, change);
            return true;
        } catch (e) {
            console.log(e);
            return false;
        }
    }

    async removeMemberToGroup(groupCn, userUid) {
        try {
            const change = {
                operation: 'delete', // add, delete, replace
                modification: {
                    memberUid: userUid
                }
            };

            await this.client.modify(`cn=${groupCn},${groupPath}`, change);
            return true;
        } catch (e) {
            console.log(e);
            return false;
        }
    }

    getCN(sn, givenName) {
        return `${givenName} ${sn}`;
    }

    async updateUser(uid, sn, givenName, password) {
        try {
            console.log(uid, sn, givenName, password);
            const change = {
                operation: 'replace', // add, delete, replace
                modification: { }
            };

            let updateCN = false;
            if (password) {
                change.modification.password = password;
            }
            if (sn) {
                change.modification.sn = sn;
                updateCN = true;
            }
            if (givenName) {
                change.modification.givenName = givenName;
                updateCN = true;
            }
            if (updateCN) {
                change.modification.cn = this.getCN(sn, givenName);
            }

            await this.client.modify(`uid=${uid},${peoplePath}`, change);

            return await this.getUser(uid);
        } catch (e) {
            console.log(e);
            return null;
        }
    }

    /**
     * @param {string} name 
     * @returns {{ok: boolean, error?: string}} Succeed
     */
    async createGroup(cn) {
        try {
            const id = await this._getNextGroupGid();
            console.log('ID', id);

            const entry = {
                objectclass: ['top', 'posixGroup'],
                cn: cn,
                gidNumber: id
            };

            await this.client.add(`cn=${cn},${groupPath}`, entry);

            return {
                ok: true,
            };
        } catch (e) {
            console.log(e);
            if (e.code === 68) {
                return {
                    ok: false,
                    error: "L'entrée existe déjà"
                };
            } else {
                return {
                    ok: false,
                    error: "Erreur LDAP"
                };
            }
        }
    }

    /**
     * 
     * @param {string} username 
     * @param {string} givenName 
     * @param {string} sn 
     * @param {string} password 
     * @returns {{ok: boolean, uid?: string, error?: string}} Succeed
     */
    async createUser(sn, givenName, password) {
        try {
            const id = await this._getNextUserId();

            const uid = givenName[0].toLowerCase() + sn.toLowerCase();

            const entry = {
                objectclass: ['top', 'person', 'organizationalPerson', 'inetOrgPerson', 'posixAccount', 'shadowAccount'],
                sn: sn,
                givenName: givenName,
                cn: this.getCN(sn, givenName),
                userPassword: password,
                homeDirectory: `/home/${uid}`,
                uidNumber: id,
                gidNumber: id,
                loginShell: '/bin/bash',
                uid: uid
            };

            await this.client.add(`uid=${uid},${peoplePath}`, entry);

            return {
                ok: true,
                uid: uid
            };
        } catch (e) {
            console.error('add failled', e);
            console.error('code erreur', e.code);

            if (e.code === 68) {
                return {
                    ok: false,
                    error: "L'entrée existe déjà"
                };
            } else {
                return {
                    ok: false,
                    error: "Erreur LDAP"
                };
            }
        }
    }

    async deleteUser(uid) {
        try {
            const groups = await this.getUserGroups(uid); // liste de cn
            for (const cn of groups) {
                await this.removeMemberToGroup(cn, uid);
            }

            await this.client.del(`uid=${uid},${peoplePath}`);
            return true;
        } catch (e) {
            console.error(e);
            return false;
        }
    }

    async deleteGroup(cn) {
        try {
            await this.client.del(`cn=${cn},${groupPath}`);
            return true;
        } catch (e) {
            console.error(e);
            return false;
        }
    }

    // TODO: générer un bon UID
    // async _getRealUidWithoutNumber(uid) {
    //     const entries = await this.client.search(peoplePath, {
    //         filter: `(uid=${uid}*)`,
    //         scope: 'sub',
    //         attributes: ['uid']
    //     });

    //     if (entries.length > 0) { // FUCK
    //         this._getRealUidWithNumber(uid + '1');
    //     }sn}`,
    //     return uid;
    // }

    // async _getRealUidWithoutNumber(uid) {
    //     const entries = await this.client.search(peoplePath, {
    //         filter: `(uid=${uid})`,
    //         scope: 'sub',
    //         attributes: ['uid']
    //     });

    //     if (entries.length > 0) { // FUCK
    //         this._getRealUidWithNumber(uid + '1');
    //     }
    //     return uid;
    // }

    /**
     * Récupérer l'id du prochain utilisateur
     */
    async _getNextUserId() {
        const entries = await this.client.search(peoplePath, {
            filter: '(objectClass=top)',
            scope: 'sub',
            attributes: ['uidNumber']
        });

        let maxId = 10000;
        for (const e of entries) {
            const uidNumber = +e.uidNumber;
            if (uidNumber > maxId) {
                maxId = uidNumber;
            }
        }

        return maxId + 1;
    }

    /**
     * Récupérer l'id du prochain utilisateur
     */
    async _getNextGroupGid() {
        console.log(groupPath);


        const entries = await this.client.search(groupPath, {
            filter: '(objectclass=top)',
            scope: 'sub',
            attributes: ['gidNumber']
        });
        console.log(entries);

        let maxId = 20000;
        for (const e of entries) {
            const gidNumber = +e.gidNumber;
            if (gidNumber > maxId) {
                maxId = gidNumber;
            }
        }

        return maxId + 1;
    }
}

const ldapService = new LdapService();
ldapService.init().then(() => console.log('Bind succeed'));

module.exports = ldapService;

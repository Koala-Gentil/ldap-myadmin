const jwt = require('jsonwebtoken');
const jwtSettings = require('./settings').jwt

class TokenService {
    generateToken(data) {
        return new Promise((res, rej) => {
            jwt.sign({
                exp: Math.floor(Date.now() / 1000) + (60 * 60) * jwtSettings.hoursBeforeExp,
                data
            }, jwtSettings.secret, { algorithm: 'HS256' }, function(err, token) {
                if (err) {
                    rej(err);
                } else {
                    res(token)
                }
            });
        });
    }

    verify(token) {
        return new Promise(res => {
            jwt.verify(token, jwtSettings.secret, function(err, decoded) {
                if (err) {
                    res(null);
                } else {
                    res(decoded);
                }
            });
        });
    }
}

const tokenService = new TokenService();
module.exports = tokenService;
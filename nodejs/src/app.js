const Koa = require('koa');
const Router = require('koa-router');
const cors = require('@koa/cors');
const bodyParser = require('koa-bodyparser');

const ldapService = require('./ldapService')
const tokenService = require('./tokenService');
const settings = require('./settings');

const app = new Koa();

app.use(cors());
app.use(bodyParser());

const public = new Router();

public.post('/token', async ctx => {
        const { username, password } = ctx.request.body;
        const token = await ldapService.signIn(username, password);
        if (token) {
            ctx.body = { token }
        } else {
            ctx.response.status = 400;
        }
    });

const private = new Router();

private.use(async (ctx, next) => {
    const auth = ctx.request.headers.authorization;
    if (auth && auth.startsWith('Bearer ')) {
        const token = auth.substring(7);
        const decoded = await tokenService.verify(token);
        if (decoded) {
            ctx.request.userData = decoded.data;
            return next();
        }
    }
    ctx.response.status = 401; // unauthorized
});

const isAdminMiddleware = async (ctx, next) => {
    const userUid = ctx.request.userData.uid
    if (await isAdmin(userUid)) {
        return next();
    } else {
        ctx.response.status = 403;
    }
}

const isAdmin = async uid => {
    const userGroups = await ldapService.getUserGroups(uid);
    return userGroups.includes(settings.ldap.adminGroup);
}

private
    .get('/data', isAdminMiddleware, async ctx => {
        const users = await ldapService.getAllUsers();
        const groups = await ldapService.getAllGroups();
        ctx.body = {
            groups, users,
        };
    })
    .get('/users', isAdminMiddleware, async ctx => {
        const users = await ldapService.getAllUsers();
        ctx.body = users;
    })
    .get('/users/:uid', async ctx => {
        const uid = ctx.params.uid;
        const userUid = ctx.request.userData.uid
        if (uid !== ctx.request.userData.uid && !(await isAdmin(userUid))) {
            ctx.status = 403
        } else {
            const user = await ldapService.getUser(uid);
            ctx.body = user;
        }
    })
    .post('/users/:uid', isAdminMiddleware, async ctx => {
        const uid = ctx.params.uid;
        const { sn, givenName, password } = ctx.request.body;
        const user = await ldapService.updateUser(uid, sn, givenName, password);

        if (user) {
            ctx.status = 200;
            ctx.body = user;
        } else {
            ctx.status = 400;
        }
    })
    .put('/users', isAdminMiddleware, async ctx => {
        const { sn, givenName, password } = ctx.request.body;
        const result = await ldapService.createUser(sn, givenName, password);
        if (result.ok) {
            ctx.status = 200;
            ctx.body = result;
        } else {
            ctx.status = 400;
            ctx.body = result;
        }
    })
    .del('/users/:uid', isAdminMiddleware, async ctx => {
        const uid = ctx.params.uid;
        const ok = await ldapService.deleteUser(uid)
        if (ok) {
            ctx.status = 204;
        } else {
            ctx.status = 404;
        }
    })
    .get('/groups', isAdminMiddleware, async ctx => {
        const groups = await ldapService.getAllGroups();
        ctx.body = groups;
    })
    .get('/groups/:cn', isAdminMiddleware, async ctx => {
        const cn = ctx.params.cn;
        const group = await ldapService.getGroup(cn);
        if (group) {
            ctx.status = 200;
            ctx.body = group;
        } else {
            ctx.status = 404;
        }
    })
    .put('/groups', isAdminMiddleware, async ctx => {
        const { cn } = ctx.request.body;
        const result = await ldapService.createGroup(cn);
        if (result.ok) {
            ctx.status = 201;
        } else {
            ctx.status = 400;
            ctx.body = result;
        }
    })
    .post('/groups/:cn/addMember/:uid', isAdminMiddleware, async ctx => {
        const { cn, uid } = ctx.params;
        const result = await ldapService.addMemberToGroup(cn, uid);
        if (result) {
            ctx.status = 201;
        } else {
            ctx.status = 400;
        }
    })
    .post('/groups/:cn/removeMember/:uid', isAdminMiddleware, async ctx => {
        const { cn, uid } = ctx.params;
        const result = await ldapService.removeMemberToGroup(cn, uid);
        if (result) {
            ctx.status = 201;
        } else {
            ctx.status = 400;
        }
    })
    .del('/groups/:cn', isAdminMiddleware, async ctx => {
        const cn = ctx.params.cn;
        const ok = await ldapService.deleteGroup(cn)
        if (ok) {
            ctx.status = 204;
        } else {
            ctx.status = 404;
        }
    });

  
app.use(public.routes())
   .use(public.allowedMethods())
   .use(private.routes())
   .use(private.allowedMethods());

app.listen(4200);

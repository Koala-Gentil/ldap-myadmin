import React from 'react';
import { BrowserRouter as Router, Switch, Route, Redirect } from 'react-router-dom';
import './App.css';
import Header from './Header';
import Login from './Login';
import Users from './Components/Users';
import Groups from './Components/Groups';
import AddUser from './Components/AddUser';
import AddGroup from './Components/AddGroup';
import GroupView from './Components/GroupView';

import 'semantic-ui-css/semantic.min.css';
import UserView from './Components/UserView';
import { useBehaviorSubject } from './useBehaviorSubject';
import { authState$ } from './authService';
import Import from './Components/Import';
import Forbiden from './Components/Forbiden';

function App() {
  const authState = useBehaviorSubject(authState$);

  const ProtectedRoute = (props) => {
    if (!authState.isLogged) {
      return <Redirect to="/login" />
    } else {
      return <Route {...props} />
    }
  }

  const AdminRoute = (props) => {
    const isAdmin = authState.role === 'ADMIN';
    return <ProtectedRoute {...props} {...(!isAdmin ? {component: Forbiden}: {})}/>
  }

  const AnonymousRoute = (props) => {
    if (authState.isLogged) {
      return <Redirect to="/" />
    } else {
      return <Route {...props} />
    }
  }

  return (
    <Router>
      <div>
        <div className="App">
          <Header />
        </div>
          <Switch>
            <AdminRoute path="/users" component={Users} />
            <AnonymousRoute path="/login" component={Login} />
            <AdminRoute path="/import" component={Import} />
            <AdminRoute path="/groups" component={Groups}/>
            <AdminRoute path="/addUser" component={AddUser}/>
            <AdminRoute path="/addGroup" component={AddGroup} />
            <AdminRoute path="/group/:cn" component={GroupView} />
            <ProtectedRoute path="/user/:uid" component={UserView} />
            <Route path='/' exact>
              {authState.isLogged && <Redirect to={`/user/${authState.uid}`} />}
            </Route>
          </Switch>
      </div>
    </Router>
  );
}

export default App;

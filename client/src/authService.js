import axios from 'axios';
import { BehaviorSubject } from 'rxjs';
import { decode } from 'jsonwebtoken';

export const authState$ = new BehaviorSubject({
    isLogged: false,
    username: null,
    cn: null,
    role: null,
    uid: null,
});

export const signIn = (token) => {
    localStorage.setItem('APP_TOKEN', token)

    const { role, username, uid, cn } = decode(token).data;

    axios.defaults.headers.common['Authorization'] = token && 'Bearer ' + token;
    authState$.next({
        isLogged: true,
        username,
        role,
        username,
        cn,
        uid,
    });
}

export const signOut = () => {
    localStorage.removeItem('APP_TOKEN');

    delete axios.defaults.headers.common['Authorization'];
    authState$.next({
        isLogged: false,
        username: null,
        cn: null,
        role: null,
        uid: null,
    });
};
    

const token = localStorage.getItem('APP_TOKEN');

if (token) {
    signIn(token);
}

import { config } from './Config';

export const apiUrl = uri => config.baseApiUrl + uri;

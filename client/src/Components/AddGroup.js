import React, { useState } from 'react';
import axios from 'axios';
import { apiUrl } from '../utils';
import { Grid, Form, Header, Segment, Button, Message, Container } from 'semantic-ui-react';

export default function AddGroup() {
    const [cn, setCn] = useState('');

    const [error, setError] = useState(null);
    const [success, setSuccess] = useState(null);
    const [isLoading, setIsLoading] = useState(false);


    const handleCreateGroup = () => {
        if (isLoading) return;

        setSuccess(null);
        setError(null);
        setIsLoading(true);
        axios.put(apiUrl('groups'), { cn })
            .then(res => {
                setIsLoading(false);
                setSuccess('Groupe créé');
            })
            .catch(error => {
                setIsLoading(false);
                setError("Erreur : " + error.response.data.error);
            });
    }

    return (
        <Grid textAlign='center'>
            <Grid.Column style={{ maxWidth: 450 }}>
                <Header as='h2' color='teal' textAlign='center'>
                    Ajouter un nouveau groupe
                </Header>
                {error && <Message negative>
                    <Message.Header>{error}</Message.Header>
                </Message>}
                {success && <Message positive>
                    <Message.Header>{success}</Message.Header>
                </Message>}
                <Form size='large'>
                    <Container textAlign='left'>
                        <Segment stacked>
                            <Form.Input fluid label='Nom' placeholder='Nom du groupe' value={cn} onChange={e => setCn(e.target.value)} />
                            <Button loading={isLoading} disabled={isLoading} onClick={handleCreateGroup} color='teal' fluid size='large'>
                                Ajouter
                            </Button>
                        </Segment>  
                    </Container>
                </Form>
            </Grid.Column>
        </Grid>
    );
}
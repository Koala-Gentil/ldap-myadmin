import React from 'react';

export default function Forbiden() {
    return <p>Désolé tu n'es pas autorisé à aller ici !</p>;
}

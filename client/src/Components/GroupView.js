import React, { useEffect, useState } from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';
import { Grid, Form, Header, Segment, Button, Message, Container, List, Icon, Select } from 'semantic-ui-react';
import { apiUrl } from '../utils';

export default function GroupView(props) {

    console.log(props);

    const cn = props.match.params.cn;

    const [group, setGroup] = useState();
    const [users, setUsers] = useState();
    const [newMember, setNewMember] = useState('');

    const [error, setError] = useState(null);
    const [success, setSuccess] = useState(null);
    const [isLoading, setIsLoading] = useState(false);

    useEffect(() => {
        axios.get(apiUrl(`groups/${cn}`))
            .then(result => {
                console.log(result);
                setGroup(result.data);
            });

        axios.get(apiUrl('users'))
            .then(result => {
                setUsers(result.data);
            });
    }, [])

    const handleAddMember = () => {
        setIsLoading(true);
        setSuccess(null);
        setError(null);
        axios.post(apiUrl(`groups/${cn}/addMember/${newMember}`))
            .then(result => {
                console.log(result);
                setGroup({ ...group, members: [...group.members, newMember] });
                setSuccess('Utilisateur ajouté au groupe');
                setIsLoading(false);
            })
            .catch(error => {
                console.error(error.response);
                setError('Erreur : ' + error.response.data.error);
                setIsLoading(false);
            });
        setNewMember('');
    }

    const handleRemoveMember = uid => () => {
        setIsLoading(true);
        setSuccess(null);
        setError(null);
        axios.post(apiUrl(`groups/${cn}/removeMember/${uid}`))
            .then(result => {
                console.log(result);
                setSuccess('Utilisateur supprimé du groupe');
                setIsLoading(false);
                setGroup({ ...group, members: group.members.filter(m => m !== uid) });
            })
            .catch(error => {
                console.error(error.response);
                setError('Erreur : ' + error.response.data.error);
                setIsLoading(false);
            });
    }

    if (!group || !users) {
        return <p>Loading...</p>
    }

    const newMembersOptions = users && users.filter(u => !group.members.includes(u.uid)).map(u => ({
        key: u.uid,
        value: u.uid,
        text: u.cn,
    }));

    const findUser = uid => users.find(u => u.uid === uid);

    console.log('GROUP', group);

    return (
        <Grid textAlign='center'>
            <Grid.Column style={{ maxWidth: 650 }}>
                <Header as='h2' color='teal' textAlign='center'>
                    Groupe : {group.cn}
                </Header>
                {error && <Message negative>
                    <Message.Header>{error}</Message.Header>
                </Message>}
                {success && <Message positive>
                    <Message.Header>{success}</Message.Header>
                </Message>}
                <Form size='large'>
                    <Segment stacked>
                        <Container textAlign='left'>
                            <Form.Input fluid label='CN' value={group.cn} readOnly />
                            <Form.Input fluid label='DN' value={group.dn} readOnly />
                            <Form.Input fluid label='GID' value={group.gidNumber} readOnly />
                            <Segment raised>
                                <Header>Membres</Header>
                                <Container textAlign='left'>
                                    <List bulleted verticalAlign='middle'>
                                        {group.members.map(uid => {
                                            const user = findUser(uid);
                                            if (!user) return null;
                                            return (
                                                <List.Item key={user.cn}>
                                                    <List.Content floated='right'>
                                                        <Button disabled={isLoading} negative icon onClick={handleRemoveMember(user.uid)}><Icon name='trash' /></Button>
                                                        <Button primary icon as={Link} to={`/user/${user.uid}`}><Icon name='info circle' /></Button>
                                                    </List.Content>
                                                    <List.Content>{user.cn} ({uid})</List.Content>
                                                </List.Item>
                                            );
                                        })}
                                    </List>
                                    <Select placeholder='Ajouter un utilisateur' options={newMembersOptions} onChange={(e, data) => setNewMember(data.value)} />
                                    {newMember && <Button disabled={isLoading} primary onClick={handleAddMember}>Ajouter</Button>}
                                </Container>
                            </Segment>
                        </Container>
                    </Segment>
                </Form>
            </Grid.Column>
        </Grid>
    );
}
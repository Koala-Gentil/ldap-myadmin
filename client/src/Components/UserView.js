import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { apiUrl } from '../utils';
import { Grid, Form, Header, Segment, Button, Message, Container, List, Icon } from 'semantic-ui-react';
import { Link } from 'react-router-dom';
import deepEqual from 'deep-equal';


export default function UserView(props) {

    const uid = props.match.params.uid;

    const [user, setUser] = useState(null);
    const [editedUser, setEditedUser] = useState(null);

    const [error, setError] = useState(null);
    const [success, setSuccess] = useState(null);
    const [isLoading, setIsLoading] = useState(false);
    const [dirty, setDirty] = useState(false);


    useEffect(() => {
        axios.get(apiUrl(`users/${uid}`))
            .then(result => {
                const user = result.data
                setEditedUser({
                    sn: user.sn,
                    givenName: user.givenName,
                    password: '',
                });
                setUser(user);
            });
    }, [])

    if (!user) {
        return <p>Loading...</p>
    }

    const handleInputChange = key => e => {
        setEditedUser({...editedUser, [key]: e.target.value});
        setDirty(true);
    }

    const handleUpdate = () => {
        setIsLoading(true);
        axios.post(apiUrl(`users/${uid}`), editedUser)
            .then(res => {
                console.log(res);
                setUser(res.data);
                setSuccess("Mise à jour réussi");
                setIsLoading(false);
            })
            .catch(err => {
                console.log(err.response);
                setError('Erreur :(');
                setIsLoading(false);
            })
    };

    return (
        <Grid textAlign='center'>
            <Grid.Column style={{ maxWidth: 650 }}>
                <Header as='h2' color='teal' textAlign='center'>
                    Modifier un utilisateur : {user.cn}
                </Header>
                {error && <Message negative>
                    <Message.Header>{error}</Message.Header>
                </Message>}
                {success && <Message positive>
                    <Message.Header>{success}</Message.Header>
                </Message>}
                <Form size='large'>
                    <Segment stacked>
                        <Container textAlign='left'>
                            <Form.Input fluid label='Nom' placeholder='Nom' value={editedUser.sn} onChange={handleInputChange('sn')} />
                            <Form.Input fluid label='Prénom' placeholder='Prénom' value={editedUser.givenName} onChange={handleInputChange('givenName')} />
                            <Form.Input fluid label='Mot de passe' icon='lock' type='password' iconPosition='left' placeholder='Mot de passe' value={editedUser.password} onChange={handleInputChange('password')} />
                            <Form.Input fluid label='CN' value={user.cn} readOnly />
                            <Form.Input fluid label='DN' value={user.dn} readOnly />
                            <Form.Input fluid label='UID' value={user.uid} readOnly />
                            <Form.Input fluid label='GID' value={user.gidNumber} readOnly />
                            <Form.Input fluid label='Home' value={user.homeDirectory} readOnly />

                            <Segment raised>
                                <Header>Groupes</Header>
                                <Container textAlign='left'>
                                    <List bulleted verticalAlign='middle'>
                                        {user.groups.map(cn => (
                                            <List.Item key={cn}>
                                                <List.Content floated='right'>
                                                    <Button primary icon as={Link} to={`/group/${cn}`}><Icon name='info circle' /></Button>
                                                </List.Content>
                                                <List.Content>{cn}</List.Content>
                                            </List.Item>
                                        ))}
                                    </List>
                                </Container>
                            </Segment>
                            {dirty && <Button loading={isLoading} disabled={isLoading} onClick={handleUpdate} color='teal' fluid size='large'>
                                Modifier
                            </Button>}
                        </Container>
                    </Segment>
                </Form>
            </Grid.Column>
        </Grid>
    );
}
import React, { useState } from 'react';
import axios from 'axios';
import { apiUrl } from '../utils';
import { Grid, Form, Header, Segment, Button, Message, Container } from 'semantic-ui-react';

export default function AddUser() {
    const [sn, setSn] = useState('');
    const [givenName, setGivenName] = useState('');
    const [password, setPassword] = useState('');

    const [error, setError] = useState(null);
    const [success, setSuccess] = useState(null);
    const [isLoading, setIsLoading] = useState(false);

    const handleInputChange = fn => e => fn(e.target.value);

    const handleCreateUser = () => {
        if (isLoading) return;

        setSuccess(null);
        setError(null);
        setIsLoading(true);

        axios.put(apiUrl('users'), { sn, givenName, password })
            .then(res => {
                setIsLoading(false);
                setSuccess("Utilisateur ajouté avec uid : " + res.data.uid);
            })
            .catch(error => {
                setIsLoading(false);
                setError("Erreur : " + error.response.data.error);
            });
    }

    return (
        <Grid textAlign='center'>
            <Grid.Column style={{ maxWidth: 450 }}>
                <Header as='h2' color='teal' textAlign='center'>
                    Créer un nouvel utilisateur
                </Header>
                {error && <Message negative>
                    <Message.Header>{error}</Message.Header>
                </Message>}
                {success && <Message positive>
                    <Message.Header>{success}</Message.Header>
                </Message>}
                <Form size='large'>
                    <Segment stacked>
                        <Container textAlign='left'>
                            <Form.Input fluid label='Nom' placeholder='Nom' value={sn} onChange={handleInputChange(setSn)} />
                            <Form.Input fluid label='Prénom' placeholder='Prénom' value={givenName} onChange={handleInputChange(setGivenName)} />
                            <Form.Input fluid label='Mot de passe' icon='lock' type='password' iconPosition='left' placeholder='Mot de passe' value={password} onChange={handleInputChange(setPassword)} />
                            <Button loading={isLoading} disabled={isLoading} onClick={handleCreateUser} color='teal' fluid size='large'>
                                Ajouter
                            </Button>
                        </Container>
                    </Segment>
                </Form>
            </Grid.Column>
        </Grid>
    );
}
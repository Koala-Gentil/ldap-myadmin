import React, { useState, useEffect, useRef } from 'react';
import axios from 'axios';
import { apiUrl } from '../utils';
import { Grid, Form, Header, Segment, Button, Message, Container, List, Icon } from 'semantic-ui-react';
import { Link } from 'react-router-dom';


export default function Import(props) {

    const [error, setError] = useState(null);
    const [success, setSuccess] = useState(null);
    const [isLoading, setIsLoading] = useState(false);

    const fileInputRef = useRef();
    const [file, setFile] = useState(null);

    const test = e => {
        console.log(e);
        console.log(fileInputRef);
        setFile(fileInputRef.current.files[0]);
    }

    return (
        <Grid textAlign='center'>
            <Grid.Column style={{ maxWidth: 450 }}>
                <Header as='h2' color='teal' textAlign='center'>
                    Importer
                </Header>
                {error && <Message negative>
                    <Message.Header>{error}</Message.Header>
                </Message>}
                {success && <Message positive>
                    <Message.Header>{success}</Message.Header>
                </Message>}
                <Form size='large'>
                    <Segment stacked>
                        <Container textAlign='left'>
                            <Button
                                content={file ? file.name : 'Fichier import utilisateur'}
                                labelPosition="left"
                                icon="file"
                                onClick={() => fileInputRef.current.click()}
                            />
                            <input
                                ref={fileInputRef}
                                type="file"
                                hidden
                                onChange={test}
                            />
                        </Container>
                    </Segment>
                </Form>
            </Grid.Column>
        </Grid>);
}
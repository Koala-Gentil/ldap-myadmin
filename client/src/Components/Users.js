import axios from 'axios';
import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import { Button, Container, Grid, Header, Icon, List } from 'semantic-ui-react';
import { apiUrl } from '../utils';


export default function () {

    const [users, setUsers] = useState([]);

    useEffect(() => {
        console.log('ok users');
        axios.get(apiUrl('users'))
            .then(result => {
                console.log(result.data);
                setUsers(result.data);
            });
    }, []);

    const handleDelUser = uid => e => {
        e.stopPropagation();
        axios.delete(apiUrl(`users/${uid}`))
            .then(result => {
                console.log(result);
                setUsers(users.filter(u => u.uid !== uid));
            })
            .catch(error => {
                console.error(error);
            });
    }


    return (
        <Grid textAlign='center'>
            <Grid.Column style={{ maxWidth: 450 }}>
                <Header as='h2' color='teal' textAlign='center'>
                    Utilisateurs
                </Header>
                <Container textAlign='left'>
                    <List divided verticalAlign='middle'>
                        {users.map(u => (
                            <List.Item key={u.uid}>
                                <List.Content floated='right'>
                                    <Button negative icon onClick={handleDelUser(u.uid)}><Icon name='trash' /></Button>
                                    <Button primary icon as={Link} to={`/user/${u.uid}`}><Icon name='info circle' /></Button>
                                </List.Content>
                                <List.Header>{u.cn} ({u.uid})</List.Header>
                                <List.Content>{u.dn}</List.Content>  
                            </List.Item>
                        ))}
                    </List>
                </Container>
            </Grid.Column>
        </Grid>
    )
}

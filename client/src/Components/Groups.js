import axios from 'axios';
import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import { Button, Container, Grid, Header, Icon, List } from 'semantic-ui-react';
import { apiUrl } from '../utils';

export default function Groups() {
    const [groups, setGroups] = useState([]);

    useEffect(() => {
        console.log('ok groups');
        axios.get(apiUrl('groups'))
            .then(result => {
                console.log(result);
                setGroups(result.data);
            });
    }, [])

    const handleRemoveGroup = cn => () => {
        axios.delete(apiUrl(`groups/${cn}`))
            .then(result => {
                console.log(result);
                setGroups(groups.filter(g => g.cn !== cn));
            })
            .catch(error => {
                console.error(error);
            });
    }

    return (
        <Grid textAlign='center'>
            <Grid.Column style={{ maxWidth: 450 }}>
                <Header as='h2' color='teal' textAlign='center'>
                    Groupes
                </Header>
                <Container textAlign='left'>
                    <List divided verticalAlign='middle'>
                        {groups.map(g => (
                            <List.Item key={g.cn}>
                                <List.Content floated='right'>
                                    <Button negative icon onClick={handleRemoveGroup(g.cn)}><Icon name='trash' /></Button>
                                    <Button primary icon as={Link} to={`/group/${g.cn}`}><Icon name='info circle' /></Button>
                                </List.Content>
                                <List.Header>{g.cn}</List.Header>
                                <List.Content>{g.dn}</List.Content>  
                            </List.Item>
                        ))}
                    </List>
                </Container>
            </Grid.Column>
        </Grid>
    );
}
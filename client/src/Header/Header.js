import axios from 'axios';
import FileSaver from 'file-saver';
import React from 'react';
import { Link } from 'react-router-dom';
import { Button, Container, Menu, Segment } from 'semantic-ui-react';
import { authState$, signOut } from '../authService';
import { useBehaviorSubject } from '../useBehaviorSubject';
import { apiUrl } from '../utils';
import './Header.css';

export default function (props) {

    const authState = useBehaviorSubject(authState$);
    const isLogged = authState.isLogged;
    const isAdmin = authState.role === 'ADMIN';
    const userUid = authState.uid;

    const exportHandler = () => {
        axios.get(apiUrl('data')).then(result => {
            console.log(result);
            const blob = new Blob([JSON.stringify(result.data)], {
                type: "text/plain;charset=utf-8"
            });
            FileSaver.saveAs(blob, 'data.json');
        });
    }

    return (
        <Segment
            textAlign='center'
            vertical
        >
            <Menu
                pointing
                secondary
                size='large'
            >
                <Container>
                    <Menu.Item header>LDAP Manager</Menu.Item>
                    {isLogged && <Menu.Item as={Link} to={`/user/${userUid}`}>Mon profil</Menu.Item>}
                    {isAdmin && <Menu.Item as={Link} to='/users'>Utilisateurs</Menu.Item>}
                    {isAdmin && <Menu.Item as={Link} to='/groups'>Groupes</Menu.Item>}
                    {isAdmin && <Menu.Item as={Link} to='/addUser'>Créer un utilisateur</Menu.Item>}
                    {isAdmin && <Menu.Item as={Link} to='/addGroup'>Ajouter un groupe</Menu.Item>}
                    {isAdmin && <Menu.Item as={Link} to='/import'>Importer</Menu.Item>}
                    {isAdmin && <Menu.Item><Button onClick={exportHandler}>Exporter</Button></Menu.Item>}
                    <Menu.Item position='right'>
                        {!isLogged && <Button color='teal' as={Link} to='/login'>
                            Se connecter
                        </Button>}
                        {isLogged && 
                            <>
                                <Menu.Item style={authState.role === 'ADMIN' ? { color: 'red' } : null}>
                                    {authState.cn}
                                </Menu.Item>
                                <Button color='red' onClick={signOut}>
                                    Se déconnecter
                                </Button>
                            </>
                        }
                    </Menu.Item>
                </Container>
            </Menu>
        </Segment>
    );
}
import React, { useState } from 'react';
import axios from 'axios';
import { useHistory } from 'react-router-dom';
import { signIn } from '../authService';
import { apiUrl } from '../utils';
import { Button, Form, Grid, Header, Image, Message, Segment } from 'semantic-ui-react'

export default function(props) {

    const history = useHistory();

    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');


    const loginHandler = (e) => {
        e.preventDefault();

        axios.post(apiUrl('token'), { username, password })
            .then(result => {
                const token = result.data.token;
                signIn(token);
                history.push('/');
            })
            .catch(err => {
                console.error(err);
            })
    }
    
    return (
        <Grid textAlign='center' verticalAlign='middle'>
            <Grid.Column style={{ maxWidth: 450 }}>
                <Header as='h2' color='teal' textAlign='center'>
                    <Image src='https://cdn141.picsart.com/307278817007211.png?type=webp&to=min&r=640' /> Se connecter
                </Header>
                <Form size='large'>
                    <Segment stacked>
                    <Form.Input fluid icon='user' iconPosition='left' placeholder='Username' value={username} onChange={e => setUsername(e.target.value)} />
                    <Form.Input
                        fluid
                        icon='lock'
                        iconPosition='left'
                        placeholder='Password'
                        type='password'
                        value={password} onChange={e => setPassword(e.target.value)} 
                    />

                    <Button onClick={loginHandler} color='teal' fluid size='large'>
                        Login
                    </Button>
                    </Segment>
                </Form>
            </Grid.Column>
        </Grid>
    );
}
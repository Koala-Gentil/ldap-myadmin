import { config as dev } from './config.dev';
import { config as prod } from './config.prod';

export const config = process.env === 'development' ? dev : prod;

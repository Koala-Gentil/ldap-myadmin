import { useState, useEffect } from "react";

export function useBehaviorSubject(bs) {
    const [val, setVal] = useState(bs.value);

    useEffect(() => {
        const sub = bs.subscribe(newValue => {
            setVal(newValue);
        });

        return () => sub.unsubscribe();
    });

    return val;
}

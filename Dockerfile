FROM node:current-slim

WORKDIR /usr/src/app
COPY nodejs/package.json .
RUN npm install

CMD [ "npm", "start" ]

COPY nodejs .